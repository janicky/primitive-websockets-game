class Player {
  constructor(id) {
      this.id = id;
      this.x = (Math.random() * 900) + 100;
      this.y = (Math.random() * 400) + 100;
      this.radius = 20;
  }

  getId() {
    return this.id;
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  getRadius() {
    return this.radius;
  }

  move (dx, dy, aw, ah) {
    if (this.getX() + dx <= this.getRadius()) {
      this.x = this.getRadius() + 10;
      return false;
    }
    if (this.getX() + dx + this.getRadius() >= aw) {
      this.x = aw - (this.getRadius() + 10);
      return false;
    }
    if (this.getY() + dy <= this.getRadius()) {
      this.y = this.getRadius() + 10;
      return false;
    }
    if (this.getY() + dy + this.getRadius() >= ah) {
      this.y = ah - (this.getRadius() + 10);
      return false;
    }
    this.x += dx;
    this.y += dy;
  }

  isCollision(c) {
    return Math.pow(c.getX() - this.getX(), 2)
           + Math.pow(c.getY() - this.getY(), 2)
           <= Math.pow(c.getRadius() + this.getRadius(), 2);
  }

}

module.exports = Player;
