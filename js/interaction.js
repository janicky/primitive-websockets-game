var map = {};
onkeydown = onkeyup = function(e) {
  e = e || event;
  map[e.keyCode] = e.type == 'keydown';
}

setInterval(function() {
  var dx = 0;
  var dy = 0;
  var speed = 3;
  if (map[38]) { // up
    dy -= 1 * speed;
  }
  if (map[40]) { // down
    dy += 1 * speed;
  }
  if (map[37]) { // left
    dx -= 1 * speed;
  }
  if (map[39]) { // right
    dx += 1 * speed;
  }

  if (dx != 0 || dy != 0) {
    socket.emit('movement', { dx: dx, dy: dy });
    console.log('emited');
  }
}, 20);
