
  var area = document.getElementById('area');
  var ctx = area.getContext('2d');

  function drawBall(x, y, radius, color) {
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2*Math.PI);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.closePath();
  }

  socket.on('status', function(data) {
    players = data.players;
    drawBalls();
  });

  function drawBalls() {
    ctx.clearRect(0, 0, area.width, area.height);
    //bg();
    players.forEach(function(item, index) {
      var color = 'gray';
      if (item.id == me.id) {
        color = 'yellow';
      }
      drawBall(item.x, item.y, 20, color);
    });
  }

  function bg() {
      ctx.beginPath();
      ctx.rect(0, 0, area.width, area.height);
      ctx.fillStyle = '#000';
      ctx.fill();
      ctx.closePath();
  }
