const Player = require('./model/Player');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.use('/js', express.static(__dirname + '/js'));

var active_players = [];

io.on('connection', function(socket){
  var me = new Player(socket.id);
  active_players.push(me);
  socket.emit('me', me);
  sendPlayers(socket);

  socket.on('disconnect', function() {
    active_players = active_players.filter(function(x) {
      return x.getId() !== socket.id;
    });
    sendPlayers(socket);
  });

  socket.on('movement', function(data) {
    me.move(data.dx, data.dy, 1000, 500);
    active_players.forEach(function(item, index){
      if (me.id !== item.id && me.isCollision(item)) {
        var ndx = data.dx / 2;
        var ndy = data.dy / 2;
        item.move(ndx, ndy, 1000, 500);
        me.move(-ndx, -ndy, 1000, 500);
        return false;
      }
    });
    active_players = active_players.map(function(x) {
      return (x.getId() == me.getId() ? me : x);
    });
    sendPlayers();
  });

});

http.listen(port, function(){
  console.log('listening on *:' + port);
});

function sendPlayers(socket) {
  io.sockets.emit('status', { players: active_players });
}
